name := """base"""

version := "1.0-SNAPSHOT"

lazy val base = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  evolutions,
  javaJdbc,
  cache,
  javaJpa,
  javaWs,
  "commons-io" % "commons-io" % "2.4",
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  "org.codehaus.jackson" % "jackson-core-asl" % "1.1.0",
  "info.cukes" % "cucumber-java" % "1.2.4",
  "info.cukes" % "cucumber-junit" % "1.2.4",
  "org.apache.poi" % "poi-ooxml" % "3.14",
  "org.apache.poi" % "poi-ooxml-schemas" % "3.14",
  "org.apache.poi" % "ooxml-schemas" % "1.3",
  "org.apache.poi" % "poi-excelant" % "3.14",
  "org.apache.poi" % "poi" % "3.7",
  "net.sf.jasperreports" % "jasperreports" % "6.2.2",
  "net.sf.jasperreports" % "jasperreports-fonts" % "6.0.0",
  "com.jayway.jsonpath" % "json-path" % "2.2.0",
  "org.apache.odftoolkit" % "simple-odf" % "0.8.1-incubating" exclude("org.slf4j","slf4j-log4j12"),
  "com.codeborne" % "phantomjsdriver" % "1.3.0",
  "org.seleniumhq.selenium" % "htmlunit-driver" % "2.21",
  //Play_pac4j
  "org.pac4j" % "play-pac4j" % "2.6.2",
  "org.pac4j" % "pac4j-http" % "1.9.5",
  "org.pac4j" % "pac4j-jwt" % "1.9.5" exclude("commons-io", "commons-io"),
  "com.google.appengine" % "appengine-endpoints" % "1.9.49",
  "org.apache.chemistry.opencmis" % "chemistry-opencmis-client-api" % "1.0.0",
  "org.apache.chemistry.opencmis" % "chemistry-opencmis-client-impl" % "1.0.0",
  "org.apache.tika" % "tika-core" % "1.14",
  "sax" % "sax" % "2.0.1"
)
