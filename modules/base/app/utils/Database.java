//Classe que gere o aprovisionamento da base de dados
package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.typesafe.config.ConfigFactory;

import play.Logger;

public class Database {

    private final static String DATABASE = "snc";
    private final static String DATABASE_USERNAME = ConfigFactory.load()
            .getString("db.default.username");
    private final static String DATABASE_PASSWORD = ConfigFactory.load()
            .getString("db.default.password");
    private final static String DATABASE_URL = ConfigFactory.load()
            .getString("db.default.url");
    private final static String DIR_SCRIPT_APROVIS_GERAL = "/vagrant/backend/conf/data/provisioning/base";
    private final static String CHAVE_MODO_CONFIGURACAO = "provisioning.mode";
    private final static String DIR_FILES_APROVIS_PERSONALIZADO = "/vagrant/backend/conf/data/provisioning/personalizado/";
    private final static String DIR_CONFIG_APROVIS_PERSONALIZADO = "/vagrant/backend/conf/provisioning.conf";
    private final static String CHAVE_APROVIS_TABELAS_PERSONALIZADO = "provisioning.tabelas.personalizado";
    private final static String DIR_CONFIG_APROVIS_BASE = "/vagrant/backend/conf/application.conf";
    private final static String CHAVE_APROVIS_TABELAS_BASE = "provisioning.tabelas.base";
    private final static String MSG_MODO_APROVIS = "Início do aprovisionamento no modo: ";
    private static final int MODO_APROVIS_NENHUM = 0;
    private static final int MODO_APROVIS_BASE = 1;
    private static final int MODO_APROVIS_BASE_E_PERSONALIZADO = 2;
    private static final int MODO_APROVIS_PERSONALIZADO = 3;
    private static final String MSG_MODO_APROVIS_NENHUM = MSG_MODO_APROVIS
            + MODO_APROVIS_NENHUM
            + " - não é efectuado qualquer aprovisionamento.";
    private static final String MSG_MODO_APROVIS_BASE = MSG_MODO_APROVIS
            + MODO_APROVIS_BASE
            + " - é efectuada a limpeza de todas as tabelas e o aprovisionamento das tabelas base.";
    private static final String MSG_MODO_APROVIS_BASE_E_PERSONALIZADO = MSG_MODO_APROVIS
            + MODO_APROVIS_BASE_E_PERSONALIZADO
            + " - é efectuada a limpeza de todas as tabelas e o aprovisionamento das tabelas base e das tabelas personalizadas.";
    private static final String MSG_MODO_APROVIS_PERSONALIZADO = MSG_MODO_APROVIS
            + MODO_APROVIS_PERSONALIZADO
            + " - é efectuada a limpeza e o aprovisionamento das tabelas personalizadas.";

    private static Process runtimeProcess;
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;
    private static BufferedReader bufferedReader;

    /**
     *
     * Método para aprovisionamento da base de dados.
     *
     * @author AIRC
     *
     */
    public void aprovisionaBaseDados() throws Exception {
        int modo;
        try {
            modo = ConfigFactory
                    .parseFile(new File(DIR_CONFIG_APROVIS_PERSONALIZADO))
                    .getInt(CHAVE_MODO_CONFIGURACAO);
        } finally {
            fechaRecursos();
        }
        switch (modo) {
        case MODO_APROVIS_NENHUM:
            Logger.info(MSG_MODO_APROVIS_NENHUM);
            verificaPreenchimentoTabelas(DIR_CONFIG_APROVIS_BASE,
                    CHAVE_APROVIS_TABELAS_BASE);
            break;
        case MODO_APROVIS_BASE:
            Logger.info(MSG_MODO_APROVIS_BASE);
            fazAprovisionamentoBase();
            verificaPreenchimentoTabelas(DIR_CONFIG_APROVIS_BASE,
                    CHAVE_APROVIS_TABELAS_BASE);
            break;
        case MODO_APROVIS_BASE_E_PERSONALIZADO:
            Logger.info(MSG_MODO_APROVIS_BASE_E_PERSONALIZADO);
            fazAprovisionamentoBase();
            verificaPreenchimentoTabelas(DIR_CONFIG_APROVIS_BASE,
                    CHAVE_APROVIS_TABELAS_BASE);
            fazAprovisionamentoPersonalizado();
            verificaPreenchimentoTabelas(DIR_CONFIG_APROVIS_PERSONALIZADO,
                    CHAVE_APROVIS_TABELAS_PERSONALIZADO);
            break;
        case MODO_APROVIS_PERSONALIZADO:
            Logger.info(MSG_MODO_APROVIS_PERSONALIZADO);
            fazAprovisionamentoPersonalizado();
            verificaPreenchimentoTabelas(DIR_CONFIG_APROVIS_PERSONALIZADO,
                    CHAVE_APROVIS_TABELAS_PERSONALIZADO);
            break;
        }
        Logger.info(
                "Finalizada a operação de aprovisionamento da base de dados.\n\n");
    }

    // Método que loga o número de registos de cada tabela aprovisionada
    // Recebe o diretório onde estão os ficheiros e a chave onde constam as
    // tabelas a aprovisionar
    private void verificaPreenchimentoTabelas(String dirConfigAprovisionamento,
            String chaveConfigAprovisionamento)
            throws IOException, SQLException {
        int numTabelas = 0;
        List<String> lista;
        String comando;

        lista = obtemListaTabelas(dirConfigAprovisionamento,
                chaveConfigAprovisionamento);
        numTabelas = lista.size();
        Logger.info("Verificação do nº de registos das seguintes tabelas:");
        for (int i = 0; i < numTabelas; i++) {
            comando = "SELECT COUNT (*) AS contador FROM public." + lista.get(i)
                    + ";";
            try {
                connection = DriverManager.getConnection(DATABASE_URL,
                        DATABASE_USERNAME, DATABASE_PASSWORD);
                statement = connection.createStatement();
                resultSet = statement.executeQuery(comando);
                resultSet.next();
                Logger.info(
                        lista.get(i) + " ->  " + resultSet.getLong("contador"));
            } finally {
                fechaRecursos();
            }
        }
        Logger.info("Verificação do nº de registos de tabelas concluído.");
    }

    // Método para aprovisionar as tabelas base
    // Chama o script de aprovisionamento inicial (tabela utilizador) e o script
    // de aprovisionamento geral (restantes tabelas base)
    private void fazAprovisionamentoBase() throws Exception {
        aprovisionaBaseDados(DIR_SCRIPT_APROVIS_GERAL, "snc-load-geral.sql",
                true);
    }

    // Método para aprovisionar qualquer tabela da base de dados
    // (individualmente)
    private void aprovisionaBaseDados(String localizacaoFicheiroString,
            String ficheiroScript, boolean limpaDados) throws Exception {
        int resultado;
        String ficheiroPath = localizacaoFicheiroString + File.separator
                + ficheiroScript;

        if (limpaDados) {
            limpaTabelasBaseDados();
        }

        List<String> cmd = new ArrayList<>();
        cmd.add("psql");
        cmd.add("-U");
        cmd.add(DATABASE_USERNAME);
        cmd.add("-q");
        cmd.add("-d");
        cmd.add(DATABASE);
        cmd.add("-w");
        cmd.add("-f");
        cmd.add(ficheiroPath);

        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.environment().put("PGHOST", "localhost");
        try {
            Process runtimeProcess = pb.start();
            resultado = runtimeProcess.waitFor();
            if (resultado == 0) {
                Logger.info(
                        "Aprovisionamento da base de dados utilizando o ficheiro "
                                + ficheiroPath + " concluído com sucesso.");
            } else {
                String msg = "Falhou o aprovisionamento da base de dados utilizando o ficheiro "
                        + ficheiroPath + ".";
                Logger.error(msg, new Exception(msg));
            }
        } finally {
            fechaRecursos();
        }
    }

    // Método para aprovisionar as tabelas personalizadas
    // Chama o script de aprovisionamento personalizado
    private void fazAprovisionamentoPersonalizado()
            throws IOException, SQLException {
        List<String> lista;
        String comandoAprovisionamento;
        String comandoLimpezaIndividual;
        int numElementos;
        String elemento;

        lista = obtemListaTabelas(DIR_CONFIG_APROVIS_PERSONALIZADO,
                CHAVE_APROVIS_TABELAS_PERSONALIZADO);
        numElementos = lista.size();
        for (int i = 0; i < numElementos; i++) {
            elemento = lista.get(i);
            comandoLimpezaIndividual = constroiComandoLimpezaIndividual(
                    elemento);
            executaInstrucaoBdServer(comandoLimpezaIndividual);
            Logger.info("Limpeza da tabela " + elemento + " concluída.");
            comandoAprovisionamento = constroiComandoAprovisionamento(elemento);
            executaInstrucaoBdServer(comandoAprovisionamento);
            Logger.info("Aprovisionamento personalizado da tabela " + elemento
                    + " concluído.");
        }
    }

    // Método para obter lista de tabelas a aprovisionar a partir de um ficheiro
    // de configuração
    private List<String> obtemListaTabelas(String dirFicheiroConfiguracao,
            String chaveConfiguracao) throws IOException, SQLException {
        List<String> lista;
        try {
            lista = ConfigFactory.parseFile(new File(dirFicheiroConfiguracao))
                    .getStringList(chaveConfiguracao);
        } finally {
            fechaRecursos();
        }
        return lista;
    }

    // Método para construir comando SQL de aprovisionamento
    private String constroiComandoAprovisionamento(String elemento)
            throws IOException, SQLException {
        String colunas = extraiColunas(elemento);
        String comando = "copy public." + elemento + " (" + colunas + ") from '"
                + DIR_FILES_APROVIS_PERSONALIZADO + elemento
                + ".txt' using delimiters '|' with csv header null as '';";
        return comando;
    }

    // Método para construir comando SQL de limpeza individual de tabelas
    private String constroiComandoLimpezaIndividual(String elemento) {
        String comando = "TRUNCATE TABLE public." + elemento
                + " RESTART IDENTITY CASCADE;";
        return comando;
    }

    // Método para extrair as colunas de uma entidade a partir do ficheiro de
    // aprovisionamento txt
    private String extraiColunas(String scriptFile)
            throws IOException, SQLException {
        String txt = "";
        try {
            bufferedReader = new BufferedReader(new FileReader(
                    DIR_FILES_APROVIS_PERSONALIZADO + scriptFile + ".txt"));
            txt = bufferedReader.readLine();
            bufferedReader.close();
        } finally {
            fechaRecursos();
        }
        String[] strArray = txt.split("[|]");
        txt = Arrays.toString(strArray);
        txt = txt.substring(1, txt.length() - 1);
        return txt;
    }

    // Método para executar intrução SQL direta ao motor da base de dados
    private void executaInstrucaoBdServer(String cmd)
            throws IOException, SQLException {
        if (cmd.equals("")) {
            return;
        }
        try {
            connection = DriverManager.getConnection(DATABASE_URL,
                    DATABASE_USERNAME, DATABASE_PASSWORD);
            statement = connection.createStatement();
            statement.execute(cmd);
            Logger.info(
                    "Executada a seguinte instrução no servidor de base de dados:\n"
                            + cmd);
        } catch (Exception e) {
            Logger.error(e.getMessage(), e);
        } finally {
            fechaRecursos();
        }
    }

    // Método para eliminar todos os dados de todas as tabelas da base de dados,
    // reconstruíndo os índices
    private void limpaTabelasBaseDados() throws IOException, SQLException {
        final String cmd = "CREATE OR REPLACE FUNCTION truncate_tables(username IN VARCHAR) RETURNS void AS $$"
                + "DECLARE statements CURSOR FOR SELECT tablename FROM pg_tables WHERE tableowner = username AND schemaname = 'public' AND tablename <> 'play_evolutions';"
                + "BEGIN FOR stmt IN statements "
                + "LOOP EXECUTE 'TRUNCATE TABLE ' || quote_ident(stmt.tablename) || ' RESTART IDENTITY CASCADE;';"
                + "END LOOP;" + "END;" + "$$ LANGUAGE plpgsql;"
                + "SELECT truncate_tables('" + DATABASE_USERNAME + "');"
                + "DROP FUNCTION truncate_tables(character varying);";
        executaInstrucaoBdServer(cmd);
        Logger.info(
                "Todas as tabelas da base de dados foram limpas e os índices reconstruídos.");
    }

    // Método para evitar memory leaks
    private void fechaRecursos() throws IOException, SQLException {
        if (statement != null) {
            statement.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
        if (connection != null) {
            connection.close();
        }
        if (runtimeProcess != null) {
            runtimeProcess.destroy();
        }
        if (bufferedReader != null) {
            bufferedReader.close();
        }
    }

}
