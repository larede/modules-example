package utils;

/**
 * 
 * SNCAP-800 Validar o NIF do Cliente
 * 
 * @author joao.brandao
 *
 */
public class NifUtils {
    /**
     * Verificar validade algoritmica de um Numero de Identificacao Fiscal
     * 
     * @param nif
     *            String numero de identificação fiscal
     * @return Boolean resultado da verificação do check digit
     */
    public static Boolean checkNifPT(String nif) {
        String regex = "\\d+";
        char c;
        int checkDigit = 0;

        if (nif != null && nif.length() == 9 && nif.matches(regex)) {
            c = nif.charAt(0);
            if (c == '1' || c == '2' || c == '5' || c == '6' || c == '8'
                    || c == '9') {
                checkDigit = charToInt(c) * 9;
                for (int i = 2; i <= 8; i++) {
                    checkDigit += charToInt(nif.charAt(i - 1)) * (10 - i);
                }
                checkDigit = 11 - (checkDigit % 11);
                if (checkDigit >= 10) {
                    checkDigit = 0;
                }
                if (checkDigit == charToInt(nif.charAt(8))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Converter caracter num inteiro
     * 
     * @param caracter
     *            char caracter a converter
     * @return Integer valor numerico
     */
    public static Integer charToInt(char caracter) {
        return Character.getNumericValue(caracter);
    }

}
