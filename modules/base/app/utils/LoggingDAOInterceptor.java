package utils;

import java.util.Arrays;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import play.Logger;
import play.Logger.ALogger;

public class LoggingDAOInterceptor implements MethodInterceptor {

	public Object invoke(MethodInvocation invocation) throws Throwable {
		if (Logger.isDebugEnabled()) {
			return invocation.proceed();
		} else {
			long start = System.nanoTime();

			try {
				return invocation.proceed();
			} finally {
				String message = String
						.format("Invocation of method %s() with parameters %s took %.1f ms.",
								invocation.getMethod().getName(),
								Arrays.toString(invocation.getArguments()),
								(System.nanoTime() - start) / 1000000.0);

				ALogger logger = Logger.of("sncap");
				logger.debug(message);
			}
		}
	}
}
