package utils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import base.AIRCMessage;
import exceptions.BusinessException;

/**
 *
 * @author Nuno Gonçalves
 *
 *
 */

public class SpreadSheetUtils {

    public static final String _XLS = ".xls";
    public static final String _XLSX = ".xlsx";
    public static final String XLS = "xls";
    public static final String XLSX = "xlsx";
    public static final String _ODS = ".ods";
    public static final String ODS = "ods";
    public static final int CELL_EMPTY = 3;
    public static final String REGEX_DECIMAL_NUMBER = "([0-9]+(\\.[0-9][0-9]?)?)?";
    public static final String REGEX_CODIGO = "[0-9]+";

    public static Workbook getWorkbook(InputStream inputStream,
            String excelFilePath) throws IOException, BusinessException {
        Workbook workbook = null;

        if (excelFilePath.endsWith(XLSX)) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith(XLS)) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            // ERRO_TIPO_FICHEIRO=O tipo do ficheiro indicado não corresponde a
            // um ficheiro {0}.
            throw new BusinessException("ERRO_TIPO_FICHEIRO", "excel");
        }
        return workbook;
    }

    public static int countCellsPerRow(org.odftoolkit.simple.table.Row row,
            int tamanho) {
        int numCellsWithInfo = 0;
        org.odftoolkit.simple.table.Cell cell;
        String cellType = "";

        for (int i = 0; i < tamanho; i++) {
            cell = row.getCellByIndex(i);
            cellType = cell.getValueType();
            if (cellType != null) {
                numCellsWithInfo++;
            }
        }
        return numCellsWithInfo;
    }

    public static String trataValorCodigoUploadDespesaComOrganica(
            String cellValue, int numLinha, int cellIndex,
            String nomeFolhaCalculo) throws BusinessException {
        cellValue = cellValue.replaceAll("\\s+", "");
        // verifica se é somente decimal
        if (!cellValue.matches(SpreadSheetUtils.REGEX_CODIGO)) {
            // DADOS_INCORRECTOS_FICHEIRO=Linha ''{0}'', coluna
            // ''{1}'' encontra-se incorrecta no ficheiro
            // ''{2}''.
            throw new BusinessException("DADOS_INCORRECTOS_FICHEIRO", numLinha,
                    cellIndex + 1, nomeFolhaCalculo);
        }
        return cellValue;
    }

    public static BigDecimal trataValorNumericoUploadDespesaComOrganica(
            String cellValue) throws BusinessException {
        BigDecimal valor = BigDecimal.ZERO;
        if (cellValue.matches(SpreadSheetUtils.REGEX_DECIMAL_NUMBER)) {
            cellValue = cellValue.replaceAll("\\s+", "");
            if (!("").equals(cellValue)) {
                valor = new BigDecimal(cellValue);
            }
        }
        return valor;
    }

    public static String valorCelulaExcelUpload(Cell cell) {
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }
        int cellType = cell.getCellType();
        switch (cellType) {
        case Cell.CELL_TYPE_STRING:
            cellValue = cell.getStringCellValue();
            break;
        case Cell.CELL_TYPE_NUMERIC:
            Double doubleNumber = cell.getNumericCellValue();
            BigDecimal bigNumber = new BigDecimal(doubleNumber.toString());
            cellValue = String.valueOf(bigNumber);
            break;
        case Cell.CELL_TYPE_FORMULA:
            if (cell.getCachedFormulaResultType() == 0) {
                cellValue = String.valueOf(cell.getNumericCellValue());
            }
            break;
        }
        return cellValue;
    }

    public static String valorCelulaOdsUpload(
            org.odftoolkit.simple.table.Cell cell) {
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }
        String cellType = cell.getValueType();
        if (cellType != null) {
            switch (cellType) {
            case "string":
                cellValue = cell.getStringValue();
                break;
            case "float":
                Double doubleNumber = cell.getDoubleValue();
                BigDecimal bigNumber = new BigDecimal(doubleNumber.toString());
                cellValue = String.valueOf(bigNumber);
                break;
            }
        }
        return cellValue;
    }

    public static void trataErrosServicosIndividuais(BusinessException e,
            int numeroLinha, String nomeFolhaCalculo) throws BusinessException {
        String msgServico = e.getAIRCMessage().getMessage();
        // EXCEL_ERRO_COM_ERRO_SERVICO=Por favor verifique a linha
        // {0} da folha de cálculo '"'{1}'"' onde ocorreu o seguinte
        // erro:
        String msgExcel = new AIRCMessage("EXCEL_ERRO_COM_ERRO_SERVICO", numeroLinha,
                nomeFolhaCalculo).getMessage();
        throw new BusinessException(msgExcel + "  " + msgServico);
    }
}
