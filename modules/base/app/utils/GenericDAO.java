package utils;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import base.EntityBase;
import base.EntityBaseRef;
import base.InterfaceBase;
import enums.EstadoRegistoEnum;
import play.db.jpa.JPA;

/**
 * DAO generico a todas as entities da app.
 *
 * @author AIRC
 *
 */
public class GenericDAO {

    public enum Ordenacao {
        ASC, DESC
    }

    /**
     * Devolve uma entidade pela sua chave primária id
     *
     * @param classe
     *            da entidade
     * @param id
     *            do registo
     * @return registo da entidade
     */
    @SuppressWarnings("unchecked")
    public static <T> T getPorId(Class<T> clazzEntidade, Long id) {

        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.eq("id", id));

        return criteria.list().isEmpty() ? null
                : (T) detach(criteria.list().get(0));
    }

    /**
     * Devolve todos os registos de uma entidade.
     *
     * @param classe
     *            da entidade
     * @return lista de registos da entidade
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getTodos(Class<T> clazzEntidade) {
        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);

        return detach(criteria.list());
    }

    /**
     * Devolve todos os registos de uma entidade ordenado por um determinado
     * campo com uma determinada ordem.
     *
     * @param instância
     *            da entidade
     * @param campo
     *            de ordenação
     * @param ordenacao
     *            a aplicar
     * @return lista de registos da entidade
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getOrdenadoPorCampo(InterfaceBase entidade,
            String campoOrdenacao, Ordenacao ordenacao) {

        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Class<?> clazzEntidade = entidade.getClass();
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        if (ordenacao.equals(Ordenacao.ASC)) {
            criteria.addOrder(Order.asc(campoOrdenacao));
        } else {
            criteria.addOrder(Order.desc(campoOrdenacao));
        }

        return detach(criteria.list());
    }

    /**
     * Devolve todos os registos de uma entidade em função do estado.
     *
     * @param instância
     *            da entidade
     * @param estado
     *            do registo
     * @return lista de registos da entidade
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getPorEstado(EntityBase entidade,
            EstadoRegistoEnum estado) {

        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Class<? extends EntityBaseRef> clazzEntidade = entidade.getClass();
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.eq("estado", estado));

        return detach(criteria.list());
    }

    /**
     * Devolve todos os registos de uma entidade em função do estado, ordenado
     * por um determinado campo com uma determinada ordem.
     *
     * @param instância
     *            da entidade
     * @param estado
     *            do registo
     * @param campo
     *            de ordenação
     * @param ordenacao
     *            a aplicar
     * @return lista de registos da entidade
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getPorEstadoOrdenadoPorCampo(EntityBase entidade,
            EstadoRegistoEnum estado, String campoOrdenacao,
            Ordenacao ordenacao) {
        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Class<?> clazzEntidade = entidade.getClass();
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.eq("estado", estado));
        if (ordenacao.equals(Ordenacao.ASC)) {
            criteria.addOrder(Order.asc(campoOrdenacao));
        } else {
            criteria.addOrder(Order.desc(campoOrdenacao));
        }

        return detach(criteria.list());
    }

    /**
     * Remove uma entidade do contexto de persistência
     *
     * @param entidade
     *            a remover
     * @return entidade 'detached' do contexto de persistência
     */
    protected static <T> T detach(T result) {
        JPA.em().clear();
        return result;
    }

    /**
     * Verifica se uma chave primária existe associada numa determinada tabela
     * enquanto chave estrangeira
     *
     * @param id
     *            da chave primária a verificar
     * @param nome
     *            do campo de ligação
     * @param entidade
     *            a ser pesquisada
     *
     * @return Boolean com a existência ou não de registo associado à chave
     *         primária
     */
    public static Boolean temRegistosAssociados(Long chavePrimaria,
            String nomeCampoEntidade, InterfaceBase entidade) {
        Class<?> clazzEntidade = entidade.getClass();
        String campoLigacao = nomeCampoEntidade + ".id";

        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.eq(campoLigacao, chavePrimaria));
        criteria.setProjection(Projections.rowCount());

        if ((Long) criteria.uniqueResult() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica se uma chave primária existe associada numa determinada tabela
     * enquanto chave estrangeira
     *
     * @param id
     *            da chave primária a verificar
     * @param nome
     *            do campo de ligação
     * @param entidade
     *            a ser pesquisada
     * @param estado
     *            do registo a ser pesquisado
     *
     * @return Boolean com a existência ou não de registo associado à chave
     *         primária
     */
    public static Boolean temRegistosAssociados(Long chavePrimaria,
            String nomeCampoEntidade, EntityBase entidade,
            EstadoRegistoEnum estado) {

        Class<?> clazzEntidade = entidade.getClass();
        String campoLigacao = nomeCampoEntidade + ".id";

        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.eq(campoLigacao, chavePrimaria));
        criteria.add(Restrictions.eq("estado", estado));
        criteria.setProjection(Projections.rowCount());

        if ((Long) criteria.uniqueResult() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica se um determinado valor (String) já existe num determinado campo
     * (coluna) da base de dados
     *
     * @param texto
     *            a verificar a sua existência na base de dados
     * @param nome
     *            do campo a ser verificado
     * @param entidade
     *            a ser pesquisada
     *
     * @return Boolean com a existência ou não de registo na base de dados
     */
    public static Boolean jaExiste(String stringATestar,
            String nomeCampoEntidade, InterfaceBase entidade) {

        Class<?> clazzEntidade = entidade.getClass();
        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.ilike(nomeCampoEntidade, stringATestar.trim(),
                MatchMode.EXACT));
        criteria.setProjection(Projections.rowCount());

        if ((Long) criteria.uniqueResult() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica se um determinado valor (String) já existe num determinado campo
     * (coluna) da base de dados
     *
     * @param texto
     *            a verificar a sua existência na base de dados
     * @param nome
     *            do campo a ser verificado
     * @param entidade
     *            a ser pesquisada
     * @param estado
     *            do registo a ser pesquisado
     *
     * @return Boolean com a existência ou não de registo na base de dados
     */
    public static Boolean jaExiste(String stringATestar,
            String nomeCampoEntidade, EntityBase entidade,
            EstadoRegistoEnum estado) {

        Class<?> clazzEntidade = entidade.getClass();
        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.ilike(nomeCampoEntidade, stringATestar.trim(),
                MatchMode.EXACT));
        criteria.add(Restrictions.eq("estado", estado));
        criteria.setProjection(Projections.rowCount());

        if ((Long) criteria.uniqueResult() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Devolve o valor máximo da coluna id (chave primária) de uma determinada
     * entidade
     *
     * @return valor máximo da coluna id ou 0 caso não exista nenhum registo
     */
    public static Long valorMaximoId(InterfaceBase entidade) {
        Class<?> clazzEntidade = entidade.getClass();
        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.setProjection(Projections.max("id"));

        return (Long) criteria.uniqueResult();
    }

    /**
     * Devolve o valor máximo da coluna id (chave primária) de uma determinada
     * entidade
     *
     * @param estado
     *            do registo a ser pesquisado
     *
     * @return valor máximo da coluna id ou 0 caso não exista nenhum registo
     */
    public static Long valorMaximoId(EntityBase entidade,
            EstadoRegistoEnum estado) {
        Class<?> clazzEntidade = entidade.getClass();
        Session sessaoHibernate = JPA.em().unwrap(org.hibernate.Session.class);
        Criteria criteria = sessaoHibernate.createCriteria(clazzEntidade);
        criteria.add(Restrictions.eq("estado", estado));
        criteria.setProjection(Projections.max("id"));
        return (Long) criteria.uniqueResult();
    }
}
