package utils;

import java.sql.Date;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import exceptions.BusinessException;

public class DateUtils {

    public static Date dataDoTexto(String dataStr) throws BusinessException {
        if (dataStr == null) {
            return null;
        } else {
            dataStr = dataStr.trim().toLowerCase();
            if ("".equals(dataStr)) {
                return null;
            }
        }

        final String[] tokens = dataStr
                .split(Constants.ImportacaoData.REGEX_IMPORTACAO);
        final int nTokens = tokens.length;
        LocalDate localDate = null;
        DateTimeFormatter formato = null;
        if (nTokens == 1) {
            try {
                localDate = LocalDate.of(1900, 01, 01)
                        .plusDays(Long.parseLong(dataStr));
            } catch (Exception e) {
                // DATA_ERRO_CONVERSAO=Foi impossível converter o texto
                // ''{0}'' para um valor de data válido.
                throw new BusinessException("DATA_ERRO_CONVERSAO", dataStr);
            }
        } else if (nTokens == 2) {
            final String[] formatosStr = Constants.ImportacaoData.FORMATOS_SEM_DIA;
            final int listSize = formatosStr.length;
            for (int i = 0; i < listSize; i++) {
                try {
                    if (tokens[0].length() > 3) {
                        dataStr = tokens[0].substring(0, 1).toUpperCase()
                                + tokens[0].substring(1, tokens[0].length())
                                + "-" + tokens[1];
                    }
                    formato = DateTimeFormatter.ofPattern(formatosStr[i])
                            .withLocale(new Locale("pt", "PT"));
                    localDate = YearMonth.parse(dataStr, formato).atDay(1);
                    break;
                } catch (Exception e) {
                    if (i == listSize - 1) {
                        // DATA_ERRO_CONVERSAO=Foi impossível converter o texto
                        // ''{0}'' para um valor de data válido.
                        throw new BusinessException("DATA_ERRO_CONVERSAO",
                                dataStr);
                    }
                }
            }
        } else if (nTokens == 3) {
            final String[] formatosStr = Constants.ImportacaoData.FORMATOS_COM_DIA;
            final int listSize = formatosStr.length;
            for (int i = 0; i < listSize; i++) {
                try {
                    formato = DateTimeFormatter.ofPattern(formatosStr[i]);
                    localDate = LocalDate.parse(dataStr, formato);
                    break;
                } catch (Exception e) {
                    if (i == listSize - 1) {
                        throw new BusinessException("DATA_ERRO_CONVERSAO",
                                dataStr);
                    }
                }
            }
        } else {
            throw new BusinessException("DATA_ERRO_CONVERSAO", dataStr);
        }

        return Date.valueOf(localDate);
    }

    public static Date dataDoTexto(String dataStr, boolean inicioMes)
            throws BusinessException {
        Date dataSql = dataDoTexto(dataStr);
        if (dataSql == null) {
            return null;
        }
        LocalDate localDate = dataSql.toLocalDate();
        if (inicioMes) {
            localDate = YearMonth.from(localDate).atDay(1);
        } else {
            localDate = YearMonth.from(localDate).atEndOfMonth();
        }
        return Date.valueOf(localDate);
    }
}
