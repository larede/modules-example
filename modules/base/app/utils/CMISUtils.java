package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConstraintException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

import play.Environment;
import utils.Constants.DadosCMIS;

/**
 * SNCAP-735 Manutenção dos dados principais de um Cliente
 *
 */
/**
 * @author AIRC
 *
 */
@Singleton
public class CMISUtils {

    private final Session cmisSession;
    private Folder rootFolder;

    /**
     * @param environment
     * @throws UnknownHostException
     */
    @Inject
    public CMISUtils(Environment environment) throws UnknownHostException {
        this.cmisSession = this.createSession();
        this.rootFolder = cmisSession.getRootFolder();
        String folderName = null;
        if (environment.isProd()) {
            folderName = "PROD";
        } else {
            folderName = "DEV";
            this.handleRootFolder(folderName);
            String uid = new String(Base64.getEncoder().encode(
                    InetAddress.getLocalHost().getHostName().getBytes()));
            folderName = uid;
        }
        this.handleRootFolder(folderName);
    }

    /**
     * Função utilizada para aferir da pasta raíz do CMIS
     * 
     * @param folderName
     */
    private void handleRootFolder(String folderName) {
        try {
            this.rootFolder = (Folder) this.cmisSession
                    .getObjectByPath(this.rootFolder.getPath() + folderName);
        } catch (Exception e) {
            this.rootFolder = this.createFolder(folderName);
        }
    }

    /**
     * Função que estabelece a sessão do CMIS
     * 
     * @return
     */
    private Session createSession() {
        // The default factory implementation
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<>();

        // ECM user credentials
        parameter.put(SessionParameter.USER, DadosCMIS.USER_NAME);
        parameter.put(SessionParameter.PASSWORD, DadosCMIS.PASSWORD);

        // ECM connection settings
        parameter.put(SessionParameter.ATOMPUB_URL,
                DadosCMIS.ALFRSCO_ATOMPUB_URL);
        parameter.put(SessionParameter.BINDING_TYPE,
                BindingType.ATOMPUB.value());

        parameter.put(SessionParameter.REPOSITORY_ID, DadosCMIS.REPOSITORY_ID);
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS,
                DadosCMIS.OBJECT_FACTORY);

        // Create CMIS session to the repository
        Session session = sessionFactory.createSession(parameter);
        return session;
    }

    /**
     * Função para criar uma pasta nova no CMIS
     * 
     * @param folderName
     * @return
     */
    public Folder createFolder(String folderName) {
        Folder folder = null;
        try {
            folder = (Folder) this.cmisSession.getObjectByPath(
                    this.rootFolder.getPath() + "/" + folderName);
        } catch (CmisObjectNotFoundException onfe) {
            Map<String, Object> props = new HashMap<>();
            props.put("cmis:objectTypeId", DadosCMIS.CONTENT_TYPE_FOLDER);
            props.put("cmis:name", folderName);
            folder = this.rootFolder.createFolder(props);
        }

        return folder;
    }

    /**
     * Função para criar um documento novo no CMIS
     * 
     * @param tenantId
     * @param file
     * @param fileType
     * @param folderName
     * @param fileName
     * @param description
     * @return
     * @throws FileNotFoundException
     */
    public Document createDocument(Long tenantId, File file, String fileType,
            String folderName, String fileName, String description)
            throws FileNotFoundException {

        Folder folder = this.createFolder(folderName);

        Map<String, Object> props = new HashMap<>();
        props.put("cmis:name", fileName);
        props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document, P:cm:titled");
        props.put("cm:description", description);

        ContentStream contentStream = cmisSession.getObjectFactory()
                .createContentStream(fileName, file.length(), fileType,
                        new FileInputStream(file));

        Document document;
        try {
            document = folder.createDocument(props, contentStream, null);

        } catch (CmisContentAlreadyExistsException ccaee) {
            document = (Document) cmisSession
                    .getObjectByPath(folder.getPath() + "/" + fileName);
        }

        return document;
    }

    /**
     * Função para verificar a existência de um objeto (pasta, ficheiro, etc) no
     * CMIS
     * 
     * @param id
     * @return
     */
    public CmisObject objectExists(String id) {
        if (id == null) {
            return null;
        }
        try {
            return this.cmisSession.getObject(id);
        } catch (CmisObjectNotFoundException e) {
            return null;
        }
    }

    /**
     * Função para obter um ficheiro existente no CMIS
     * 
     * @param id
     * @return
     * @throws IOException
     */
    public File getFileCMIS(String id) throws IOException {
        CmisObject cmisObject = this.objectExists(id);
        if (cmisObject == null) {
            return null;
        }

        Document document = (Document) cmisObject;
        ContentStream contentStream = document.getContentStream();
        InputStream stream = contentStream.getStream();

        File file = File.createTempFile(contentStream.getFileName(), "");
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = stream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            stream.close();
            out.flush();
            out.close();
        }

        file.deleteOnExit();

        return file;

    }

    /**
     * Função para eliminar o conteúdo total de uma pasta CMIS de um determinado
     * developer. É utilizado em conjunto com o reset da base de dados.
     */
    public void deleteDeveloperOwnFolder() {
        String pathRootFolder = this.rootFolder.getPath();
        if (pathRootFolder.startsWith("/DEV")) {
            try {
                Folder devFolder = (Folder) cmisSession
                        .getObjectByPath(pathRootFolder);
                for (CmisObject cmisObject : devFolder.getChildren()) {
                    try {
                        cmisObject.delete(true);
                    } catch (CmisConstraintException cce) {
                        ((Folder) cmisObject).deleteTree(true,
                                UnfileObject.DELETE, true);
                    }
                }
            } catch (CmisObjectNotFoundException e) {
            }
        }
    }

    /**
     * Função para eliminar um objeto do CMIS
     * 
     * @param idObject
     */
    public void deleteObjectById(String idObject) {
        try {
            CmisObject object = this.cmisSession.getObject(idObject);
            object.delete(true);
        } catch (CmisObjectNotFoundException e) {
            return;
        }
    }

}
