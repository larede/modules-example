package utils;

/**
 * Constantes da aplicação.
 *
 * @author AIRC
 *
 */
public abstract class Constants {

    /**
     * Constante que define o formato do Status dos registos dos modelos.
     *
     * @author AIRC
     *
     */
    public enum StatusRegisto {
        ATIVOS, INATIVOS, TODOS
    }

    /**
     * Constante que define o formato da data na app.
     *
     */
    public static final String DATE_FORMAT = "dd-MM-yyyy";

    /**
     * Todos os serviços logam o Input / Output
     *
     * @author AIRC
     *
     */
    public static class Log {
        public static final String INPUT = "INPUT";
        public static final String OUTPUT = "OUTPUT";

        // adiçao de um construtor privado por sugestão do Sonarqube
        private Log() {
        }
    }

    /**
     * Campos persistidos em sessão.
     *
     * @author AIRC
     *
     */
    public static class Session {
        public static final String USERNAME = "username";
        public static final String TENANT_ID = "tenantId";

        private Session() {
        }
    }

    /**
     * Classe que define as constantes relativas aos formatos de importação /
     * exportação de datas.
     * 
     */
    public static class ImportacaoData {
        public static final String[] FORMATOS_SEM_DIA = { "M-yyyy", "M/yyyy",
                "MM-yyyy", "MM/yyyy", "MMM-yyyy", "MMM/yyyy", "MMMM-yyyy",
                "MMMM/yyyy" };
        public static final String[] FORMATOS_COM_DIA = { "d-M-yyyy",
                "d/M/yyyy", "dd-M-yyyy", "dd/M/yyyy", "d-MM-yyyy", "d/MM/yyyy",
                "dd-MM-yyyy", "dd/MM/yyyy" };
        public static final String REGEX_IMPORTACAO = "-|\\s+|/";
    }

    /**
     * Classe que define as constantes relativas às configurações para
     * comunicação HTTP
     * 
     */
    public static class HttpConfigs {
        public final static String URL_CONSULTA_SERVICO_CTT = "http://www.ctt.pt/pdcp/xml_pdcp?incodpos=";
        public final static String REGEX_CODIGO_INE = "^\\d{6}$";
        public final static String[] REGEX_CODIGO_POSTAL = { "^\\d{7}$",
                "^\\d{4}-\\d{3}$" };

        private HttpConfigs() {
        }
    }

    /**
     * Classe que define as constantes/dados relativas ao acesso ao CMIS
     * 
     */
    public static class DadosCMIS {

        public final static String USER_NAME = "admin";
        public final static String PASSWORD = "airc##00";
        public final static String FOLDER_NAME = "tenantId";
        public final static String ALFRSCO_ATOMPUB_URL = "http://srvlecm01.ad.airc.pt:8080/alfresco/api/-default-/public/cmis/versions/1.0/atom/";
        public final static String REPOSITORY_ID = "-default-";
        public final static String CONTENT_TYPE_DOCUMENT = "cmis:document";
        public final static String CONTENT_TYPE_FOLDER = "cmis:folder";
        // Não foi possível meter no sbt, pelo que o .jar ficou na lib, isto é
        // usado para alterar a descrição do documento gravado
        public final static String OBJECT_FACTORY = "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl";
        public final static String LOGO_DEFAULT = "6b314736-87ac-4fc6-92b1-deaefaae562b";

        private DadosCMIS() {

        }
    }

}
