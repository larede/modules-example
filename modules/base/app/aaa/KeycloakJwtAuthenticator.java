package aaa;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.TokenCredentials;
import org.pac4j.core.exception.HttpAction;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileHelper;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator;
import org.pac4j.jwt.profile.JwtProfile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.util.JSONObjectUtils;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;

import net.minidev.json.JSONObject;
import play.api.Play;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Http.Context;

public class KeycloakJwtAuthenticator extends JwtAuthenticator {

    String validationURL;

    public KeycloakJwtAuthenticator(String url) {
        validationURL = url;
    }

    @Override
    public CommonProfile validateToken(final String token) {
        final TokenCredentials credentials = new TokenCredentials(token,
                "(validateToken)Method");

        try {
            validate(credentials, null);
        } catch (HttpAction e) {
            e.printStackTrace();
        }

        return credentials.getUserProfile();
    }

    public JsonNode validateUserInKeycloak(String token)
            throws IOException, HttpAction {
        if (token == null || token.isEmpty())
            throw new TechnicalException("Invalid token");

        WSClient ws = Play.current().injector().instanceOf(WSClient.class);
        WSResponse response = null;

        try {
            response = ws.url(validationURL)
                    .setHeader("authorization", "Bearer " + token)
                    .get().toCompletableFuture().get();
        } catch (Exception e) {
            e.printStackTrace();
            ws.close();
            throw new TechnicalException("Response error", e);
        }

        String userInfo = response.getBody();
        if (userInfo.contains("error")) {
            System.out.println(userInfo);
            throw new HttpAction(userInfo, 401);
        }

        return (new ObjectMapper()).readTree(userInfo);
    }

    @Override
    public void validate(final TokenCredentials credentials,
            final WebContext context) throws HttpAction {
        final String token = credentials.getToken();

        SignedJWT signedJWT = null;

        try {
            // Parse the token
            final JWT jwt = JWTParser.parse(token);

            if (jwt instanceof SignedJWT) {
                signedJWT = (SignedJWT) jwt;

            } else {
                throw new TechnicalException("unsupported unsecured jwt");
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new TechnicalException("Cannot decrypt / verify JWT", e);
        }

        JsonNode userInfo = null;
        try {
            userInfo = validateUserInKeycloak(token);
        } catch (IOException e) {
            e.printStackTrace();
            throw new TechnicalException("Cannot validate user in Keycloak", e);
        }

        try {
            createJwtProfile(credentials, signedJWT, userInfo);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new TechnicalException("Cannot get claimSet", e);
        }
    }

    private static void createJwtProfile(final TokenCredentials credentials,
            final SignedJWT signedJWT, JsonNode userInfo)
            throws ParseException {
        final JWTClaimsSet claimSet = signedJWT.getJWTClaimsSet();
        String subject = claimSet.getSubject();

        if (!subject.contains(UserProfile.SEPARATOR)) {
            subject = JwtProfile.class.getSimpleName() + UserProfile.SEPARATOR
                    + subject;
        }

        final Map<String, Object> attributes = new HashMap<>(
                claimSet.getClaims());

        final CommonProfile profile = ProfileHelper.buildProfile(subject,
                attributes);

        String client = (String) attributes.get("azp");
        JSONObject resourceAccess = (JSONObject) attributes
                .get("resource_access");
        if (resourceAccess.containsKey(client)) {
            JSONObject appRessource = JSONObjectUtils
                    .getJSONObject(resourceAccess, client);
            List<String> appRoles = JSONObjectUtils.getStringList(appRessource,
                    "roles");
            profile.addRoles(appRoles);
        }

        Context.current().request().setUsername(
                profile.getAttributes().get("preferred_username").toString());
        credentials.setUserProfile(profile);
    }
}
