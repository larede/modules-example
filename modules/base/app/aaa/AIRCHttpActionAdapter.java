package aaa;

import static play.mvc.Results.ok;
import static play.mvc.Results.unauthorized;

import org.pac4j.core.context.HttpConstants;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.http.DefaultHttpActionAdapter;

import play.mvc.Result;

public class AIRCHttpActionAdapter extends DefaultHttpActionAdapter {

    @Override
    public Result adapt(final int code, final PlayWebContext context) {
        logger.debug("requires HTTP action: {}", code);
        if (code == HttpConstants.UNAUTHORIZED) {
            return unauthorized("authentication required");
        } else if (code == HttpConstants.FORBIDDEN) {
            return unauthorized("forbidden");
        } else if (code == HttpConstants.BAD_REQUEST) {
            return unauthorized("bad request");
        } else if (code == HttpConstants.OK) {
            final String content = context.getResponseContent();
            logger.debug("render: {}", content);
            return ok(content).as(HttpConstants.HTML_CONTENT_TYPE);
        }
        final String message = "Unsupported HTTP action: " + code;
        logger.error(message);
        throw new TechnicalException(message);
    }
}
