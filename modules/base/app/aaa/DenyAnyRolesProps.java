package aaa;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.pac4j.core.authorization.authorizer.Authorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.exception.HttpAction;
import org.pac4j.core.profile.CommonProfile;

import com.typesafe.config.ConfigFactory;

public class DenyAnyRolesProps implements Authorizer<CommonProfile> {
    String prop = "";

    public DenyAnyRolesProps(String prop) {
        this.prop = prop;

    }

    List<String> getProps() {
        return Arrays.asList(ConfigFactory.load("props.properties")
                .getString(prop).split("\\s*,\\s*"));
    }

    @Override
    public boolean isAuthorized(WebContext context,
            List<CommonProfile> profiles) throws HttpAction {
        if (profiles == null) {
            return false;
        }
        List<String> flat = profiles.stream()
                .flatMap(l -> l.getRoles().stream())
                .collect(Collectors.toList());
        return Collections.disjoint(flat, getProps());
    }
}
