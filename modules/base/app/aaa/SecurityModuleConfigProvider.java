package aaa;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Provider;

import org.pac4j.core.client.Client;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.context.session.SessionStore;
import org.pac4j.core.http.HttpActionAdapter;
import org.pac4j.http.client.direct.HeaderClient;
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator;

import play.Configuration;

public class SecurityModuleConfigProvider implements Provider<Config> {

    @Inject
    private HttpActionAdapter httpActionAdapter;

    @Inject
    private Configuration configuration;

    @Inject
    private SessionStore sessionStore;

    @Override
    public Config get() {
        Stream<Client> clientsList = configuration
                .getConfigList("keycloak.realms").stream()
                .flatMap(realmConf -> {
                    String realmName = realmConf.getString("name");

                    Stream<Client> headerClients = realmConf
                            .getConfigList("clients").stream()
                            .map(clientConf -> {
                                String clientId = clientConf.getString("name");
                                return createHeaderClient(realmName, clientId);
                            });
                    return headerClients;
                });

        Clients clients = new Clients(
                configuration.getString("play.callbackUrl"),
                clientsList.collect(Collectors.toList()));

        final Config config = new Config();

        config.setClients(clients);
        config.setHttpActionAdapter(httpActionAdapter);
        config.setSessionStore(sessionStore);
        return config;
    }

    private HeaderClient createHeaderClient(String realm, String name) {
        final String keycloakApiUrl = configuration.getString("keycloak.url");
        final String userDetailsUrl = keycloakApiUrl + realm
                + "/protocol/openid-connect/userinfo";

        HeaderClient headerClient = new HeaderClient("Authorization",
                getKeycloakJwtAuthenticator(userDetailsUrl));
        headerClient.setPrefixHeader("Bearer ");
        headerClient.setName(name);
        return headerClient;
    }

    protected JwtAuthenticator getKeycloakJwtAuthenticator(
            String userDetailsUrl) {
        return new KeycloakJwtAuthenticator(userDetailsUrl);
    }
}
