package aaa;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.pac4j.core.authorization.authorizer.Authorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.exception.HttpAction;
import org.pac4j.core.profile.CommonProfile;

import com.typesafe.config.ConfigFactory;

public class ActionAuthorizer implements Authorizer<CommonProfile> {

    List<String> getPropRoles(String Prop) {
        return Arrays.asList(ConfigFactory.load("props.properties")
                .getString(Prop).split("\\s*,\\s*"));
    }

    public boolean hasSecundVal(WebContext context,
            List<CommonProfile> profiles, String Prop) {
        if (profiles == null) {
            return false;
        }

        final List<String> profileRoles = profiles.stream()
                .flatMap(l -> l.getRoles().stream())
                .collect(Collectors.toList());

        final List<String> Roles = getPropRoles(Prop);

        return profileRoles.containsAll(Roles);
    }

    @Override
    public boolean isAuthorized(WebContext context,
            List<CommonProfile> profiles) throws HttpAction {
        return profiles != null;
    }
}
