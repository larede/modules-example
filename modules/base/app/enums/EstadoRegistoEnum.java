package enums;

import java.util.LinkedHashMap;
import java.util.Map;

import exceptions.NotFoundException;

public enum EstadoRegistoEnum {
    ATIVO(1), RECICLADO(2), ELIMINADO(3);

    public final static String parametros = "estado?=[1=ATIVO, 2=RECICLADO, 3=ELIMINADO]";
    private Integer valorBd;

    private EstadoRegistoEnum(Integer value) {
        this.valorBd = value;
    }

    private static final Map<Integer, EstadoRegistoEnum> MAPA = new LinkedHashMap<>();
    static {
        for (EstadoRegistoEnum estado : EstadoRegistoEnum.values()) {
            MAPA.put(estado.valorBd, estado);
        }
    }

    public static EstadoRegistoEnum convertToEntityAttribute(Integer intValue)
            throws NotFoundException {
        EstadoRegistoEnum estadoRegistoEnum = MAPA.get(intValue);
        if (estadoRegistoEnum == null) {
            // PARAMETRO_INEXISTENTE=Parâmetro de consulta inexistente
            // (parâmetros disponíveis: {0})
            throw new NotFoundException("PARAMETRO_INEXISTENTE", parametros);
        }
        return estadoRegistoEnum;
    }

    public static Integer convertToDatabaseColumn(
            EstadoRegistoEnum estadoRegistoEnum) throws NotFoundException {
        Integer intValue = estadoRegistoEnum.valorBd;
        if (intValue == null) {
            // PARAMETRO_INEXISTENTE=Parâmetro de consulta inexistente
            // (parâmetros disponíveis: {0})
            throw new NotFoundException("PARAMETRO_INEXISTENTE", parametros);
        }
        return intValue;
    }
}
