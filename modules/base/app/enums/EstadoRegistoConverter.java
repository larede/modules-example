package enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import exceptions.NotFoundException;

@Converter(autoApply = true)
public class EstadoRegistoConverter
        implements AttributeConverter<EstadoRegistoEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EstadoRegistoEnum value) {
        try {
            return EstadoRegistoEnum.convertToDatabaseColumn(value);
        } catch (NotFoundException e) {
            return null;
        }
    }

    @Override
    public EstadoRegistoEnum convertToEntityAttribute(Integer value) {
        try {
            return EstadoRegistoEnum.convertToEntityAttribute(value);
        } catch (NotFoundException e) {
            return null;
        }
    }

}
