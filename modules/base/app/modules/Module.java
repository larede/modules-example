package modules;

import org.pac4j.core.config.Config;
import org.pac4j.core.context.session.SessionStore;
import org.pac4j.core.http.HttpActionAdapter;
import org.pac4j.play.store.PlayCacheStore;
import org.pac4j.play.store.PlaySessionStore;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

import aaa.AIRCHttpActionAdapter;
import aaa.SecurityModuleConfigProvider;
import utils.LoggingDAO;
import utils.LoggingDAOInterceptor;

public class Module extends AbstractModule {

    @Override
    protected void configure() {
    	//bindInterceptor(
    	//		Matchers.any(),
        //        Matchers.annotatedWith(LoggingDAO.class),
        //        new LoggingDAOInterceptor());

        // Maps type (e.g. PlaySessionStore) to its implementation (e.g
        // PlayCacheStore)
        bind(PlaySessionStore.class).to(PlayCacheStore.class);

        // Binds for config instance
        bind(SessionStore.class).to(PlayCacheStore.class);
        bind(HttpActionAdapter.class).to(AIRCHttpActionAdapter.class);
        bind(Config.class).toProvider(SecurityModuleConfigProvider.class);
    }
}
