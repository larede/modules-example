package exceptions;

import static play.mvc.Http.Status.NOT_FOUND;

public class NotFoundException extends AIRCException {

    public NotFoundException(String code, Object... varargs) {
        super(code, varargs);
    }

    @Override
    public int getHttpStatus() {
        return NOT_FOUND;
    }
}
