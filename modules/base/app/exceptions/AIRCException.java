package exceptions;

import static play.mvc.Http.Status.INTERNAL_SERVER_ERROR;

import base.AIRCMessage;
import play.libs.Json;

public abstract class AIRCException extends Exception {
    private static final long serialVersionUID = 1L;
    protected int httpStatus = INTERNAL_SERVER_ERROR;

    public AIRCException(String code, Object... varargs) {
        super((new AIRCMessage(code, varargs)).toJson().toString());
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public AIRCMessage getAIRCMessage() {
        return Json.fromJson(Json.parse(this.getMessage()), AIRCMessage.class);
    }
}
