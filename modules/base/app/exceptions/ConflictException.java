package exceptions;

import static play.mvc.Http.Status.CONFLICT;

public class ConflictException extends AIRCException {

    public ConflictException(String code, Object... varargs) {
        super(code, varargs);
    }

    @Override
    public int getHttpStatus() {
        return CONFLICT;
    }
}
