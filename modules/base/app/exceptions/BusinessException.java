package exceptions;

import static play.mvc.Http.Status.PAYMENT_REQUIRED;

import base.AIRCMessage;

public class BusinessException extends AIRCException {

    public BusinessException(String code, Object... varargs) {
        super(code, varargs);
    }

    public BusinessException(AIRCMessage message) {
        super(message.toJson().toString());
    }

    @Override
    public int getHttpStatus() {
        return PAYMENT_REQUIRED;
    }
}
