package services;

import static play.mvc.Http.Status.FORBIDDEN;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.pac4j.play.java.Secure;
import org.slf4j.MDC;

import play.Logger;
import play.Logger.ALogger;
import play.api.Play;
import play.core.j.JavaResultExtractor;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Http.Cookie;
import utils.Constants;
import play.mvc.Result;
import play.mvc.Results;
import akka.stream.Materializer;
import akka.util.ByteString;
import base.AIRCMessage;
import exceptions.AIRCException;
import models.tenant.Tenant;
import models.tenant.TenantDAO;

import com.fasterxml.jackson.databind.JsonNode;

public class RequestInterceptor implements play.http.ActionCreator {
	ALogger log = Logger.of("sncap");

	Materializer mat;

	@Inject
	public RequestInterceptor(Materializer mat) {
		this.mat = mat;
	}

	@Override
	public Action createAction(Http.Request request, Method actionMethod) {
		return new Action.Simple() {
			@Override
			public CompletionStage<Result> call(Http.Context ctx) {
				CompletionStage<Result> result = null;
				long start = System.nanoTime();

				String user = ctx.request().username();
				String method = ctx.request().method() + " "
						+ ctx.request().uri();

				Cookie cookie = ctx.request().cookie("PLAY_SESSION");
				MDC.put("session", cookie == null ? null : cookie.value());
				MDC.put("user", user);
				MDC.put("request", Long.toString(ctx._requestHeader().id()));
				try {
					logInput(ctx, method);

					boolean hasError = false;
					if (!actionMethod.isAnnotationPresent(Secure.class)) {
						result = delegate.call(ctx);
					} else {
						List<Tenant> listTenants = TenantDAO.getTenantsByUser(user);
						if (listTenants == null || listTenants.size() == 0) {
							result = CompletableFuture.completedFuture(Results
									.status(FORBIDDEN, AIRCMessage
											.toJson("SEM_TENANT_PARA_LOGIN")));
						} else {
							setTenant(listTenants);

							try {
								result = delegate.call(ctx);
							} catch (Exception e) {
								if (e instanceof AIRCException) {
									try {
										EntityTransaction tx = JPA.em()
												.getTransaction();
										if (tx.isActive()) {
											tx.setRollbackOnly();
										}
									} catch (Exception ex2) {
										throw ex2;
									}

									AIRCException aircException = (AIRCException) e;
									hasError = true;

									result = CompletableFuture
											.completedFuture(Results.status(
													aircException
															.getHttpStatus(),
													Json.parse(aircException
															.getMessage())));

								} else {
									throw e;
								}
							}
						}
					}

					logOutput(ctx, method, result, hasError, start);
				} finally {
					MDC.remove("session");
					MDC.remove("user");
					MDC.remove("request");
				}

				return result;
			}
		};
	}

	private void setTenant(List<Tenant> listTenants) {
		String tenant = Context.current().session()
				.get(Constants.Session.TENANT_ID);
		// Assume default
		if (tenant == null || "".equals(tenant)) {
			tenant = listTenants.get(0).getId().toString();
			Context.current().session()
					.put(Constants.Session.TENANT_ID, tenant);
		}

		Session session = (Session) JPA.em().getDelegate();
		Filter filter = session.enableFilter("multitenant");
		final Long tenantId = new Long(tenant);

		// TO-DO Se tenantId que se encontra na session não é válido volta a
		// assumir default
		if (listTenants.stream().anyMatch(t -> t.getId().equals(tenantId))) {
			filter.setParameter("tenant_id", tenantId);
		} else {
			final Long newTenantId = listTenants.get(0).getId();
			Context.current().session()
					.put(Constants.Session.TENANT_ID, newTenantId.toString());

			filter.setParameter("tenant_id", newTenantId);
		}
	}

	private void logInput(Http.Context ctx, String method) {
		String message = String.format("[%s] [INPUT]", method);
		if (log.isDebugEnabled()) {
			JsonNode inputJson = ctx.request().body().asJson();
			message += String.format(" %s", inputJson == null ? "" : inputJson);
		}
		log.info(message);
	}

	private void logOutput(Http.Context ctx, String method,
			CompletionStage<Result> result, boolean hasError, long start) {
		double time = (System.nanoTime() - start) / 1000000.0;

		String message = String.format("[%s] [OUTPUT] (%.1fms)", method, time);
		if (result != null) {
			Result response = (((CompletableFuture<Result>) result).join());

			message += String.format(" %s ",
					Integer.toString(response.status()));
			String contentType = response.contentType().get();
			if ((log.isTraceEnabled() || hasError)
					&& contentType.equals("application/json")) {
				ByteString body = JavaResultExtractor.getBody(response, 10000l,
						mat);

				message += body.decodeString("UTF-8");
			} else {
				message += contentType;
			}
		}

		if (hasError) {
			log.error(message);
		} else {
			log.info(message);
		}
	}
}
