package services;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.pac4j.core.exception.TechnicalException;

import exceptions.AIRCException;
import play.Configuration;
import play.Environment;
import play.api.OptionalSourceMapper;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.libs.Json;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;

@Singleton
public class ErrorHandler extends DefaultHttpErrorHandler {

    @Inject
    public ErrorHandler(Configuration configuration, Environment environment,
            OptionalSourceMapper sourceMapper, Provider<Router> routes) {
        super(configuration, environment, sourceMapper, routes);
    }

    @Override
    public CompletionStage<Result> onServerError(RequestHeader request,
            Throwable exception) {
        if (exception instanceof AIRCException) {
            return getAIRCResult(exception);
        } else if (exception.getCause() != null
                && exception.getCause() instanceof TechnicalException
                && exception.getCause().getCause() != null
                && exception.getCause().getCause() instanceof AIRCException) {
            return getAIRCResult(exception.getCause().getCause());
        } else if (exception.getCause() != null
                && exception.getCause().getCause() != null
                && exception.getCause() instanceof TechnicalException) {
            return super.onServerError(request,
                    exception.getCause().getCause());
        } else {
            return super.onServerError(request, exception);
        }
    }

    private CompletionStage<Result> getAIRCResult(Throwable exception) {
        AIRCException aircException = (AIRCException) exception;

        return CompletableFuture
                .completedFuture(Results.status(aircException.getHttpStatus(),
                        Json.parse(aircException.getMessage())));
    }
}
