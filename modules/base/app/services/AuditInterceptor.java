package services;

import java.io.Serializable;
import java.sql.Date;

import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import base.EntityBaseRef;
import play.Logger;
import play.Logger.ALogger;
import play.mvc.Http.Context;
import utils.Constants;

public class AuditInterceptor extends EmptyInterceptor {

    private int updates;
    private int creates;
    private int loads;
    ALogger logger = Logger.of("sncap");

    @Override
    public void onDelete(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {
        // do nothing
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id,
            Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) {

        if (entity instanceof EntityBaseRef) {
            updates++;
            for (int i = 0; i < propertyNames.length; i++) {
                if ("timestampUpdated".equals(propertyNames[i])) {
                    currentState[i] = new Date(System.currentTimeMillis());
                } else if ("userUpdated".equals(propertyNames[i])) {
                    currentState[i] = Context.current().request().username();
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public boolean onLoad(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {

        if (entity instanceof EntityBaseRef) {
            loads++;
            return true;
        }

        return false;
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {

        if (entity instanceof EntityBaseRef) {
            creates++;
            for (int i = 0; i < propertyNames.length; i++) {
                if ("timestampUpdated".equals(propertyNames[i])) {
                    state[i] = new Date(System.currentTimeMillis());
                } else if ("userUpdated".equals(propertyNames[i])) {
                    state[i] = Context.current().request().username();
                } else if ("tenantId".equals(propertyNames[i])) {
                    state[i] = Long.parseLong(Context.current().session()
                            .get(Constants.Session.TENANT_ID));
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public void afterTransactionCompletion(Transaction tx) {
        //logger.info("Creations: " + creates + ", Updates: " + updates
        //        + ", Loads: " + loads);

        updates = 0;
        creates = 0;
        loads = 0;
    }
}
