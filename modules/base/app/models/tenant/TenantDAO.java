package models.tenant;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import exceptions.ConflictException;
import play.db.jpa.JPA;
import utils.GenericDAO;
import utils.LoggingDAO;

public class TenantDAO extends GenericDAO {

	@LoggingDAO
	public static List<Tenant> getTenantsByUser(String user) {
		return JPA.em().createNamedQuery(Tenant.GET_TENANTS_POR_USER)
				.setParameter("user", "%" + user + ",%").getResultList();
	}
    /**
     * Verifica a existencia de um registo associado a um tenant
     *
     * @param tenantID
     * @throws ConflictException
     */
    public static Tenant getPorTenantId(Long id) throws ConflictException {
        Criteria criteria;
        try {
            Session sessaoHibernate = JPA.em()
                    .unwrap(org.hibernate.Session.class);
            criteria = sessaoHibernate.createCriteria(Tenant.class);
            criteria.add(Restrictions.eq("id", id));
        } catch (Exception e) {
            throw new ConflictException(e.getMessage());
        }
        return detach((Tenant) criteria.uniqueResult());
    }
}
