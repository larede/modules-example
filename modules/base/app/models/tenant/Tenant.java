package models.tenant;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import base.InterfaceBase;
import enums.EstadoRegistoConverter;
import enums.EstadoRegistoEnum;
import play.db.jpa.JPA;

/***
 * SNCAP-673 - carregamento-base-das-Tenants
 *
 * @author AIRC
 *
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Tenant.GET_TENANTS_POR_USER, query = "FROM Tenant WHERE users LIKE :user") })
@Table(name = "geral_cliente")
public class Tenant extends InterfaceBase {

    public static final String GET_TENANTS_POR_USER = "Tenant.GET_TENANTS_POR_USER";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic
    @Column(nullable = false, insertable = true, updatable = true, columnDefinition = "integer default 1")
    @JsonIgnore
    @Convert(converter = EstadoRegistoConverter.class)
    private EstadoRegistoEnum estado = EstadoRegistoEnum.ATIVO;

    @Basic
    @Column(nullable = false, insertable = true, updatable = false, length = 50)
    private String nome;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 255)
    private String users;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 15)
    private String nif;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 250)
    private String morada;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 10)
    private String codigoPostal;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 250)
    private String concelho;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 250)
    private String freguesia;

    @Basic
    @Column(nullable = true, insertable = true, updatable = true, length = 250)
    private String logotipoId;

    @Transient
    private String localidade;

    public Tenant() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoRegistoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoRegistoEnum estado) {
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public String[] getUsers() {
        return users.split(",");
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setUsers(String[] users) {
        StringBuilder sbUsers = new StringBuilder();
        for (String user : users) {
            sbUsers.append(user.trim() + ",");
        }

        this.users = sbUsers.toString();
    }

    /**
     * @return the nif
     */
    public String getNif() {
        return nif;
    }

    /**
     * @param nif
     *            the nif to set
     */
    public void setNif(String nif) {
        this.nif = nif;
    }

    /**
     * @return the morada
     */
    public String getMorada() {
        return morada;
    }

    /**
     * @param morada
     *            the morada to set
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal
     *            the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the concelho
     */
    public String getConcelho() {
        return concelho;
    }

    /**
     * @param concelho
     *            the concelho to set
     */
    public void setConcelho(String concelho) {
        this.concelho = concelho;
    }

    /**
     * @return the freguesia
     */
    public String getFreguesia() {
        return freguesia;
    }

    /**
     * @param freguesia
     *            the freguesia to set
     */
    public void setFreguesia(String freguesia) {
        this.freguesia = freguesia;
    }

    /**
     * @return the logotipoId
     */
    public String getLogotipoId() {
        return logotipoId;
    }

    /**
     * @param logotipoId
     *            the logotipoId to set
     */
    public void setLogotipoId(String logotipoId) {
        this.logotipoId = logotipoId;
    }

    /**
     * @return the localidade
     */
    public String getLocalidade() {
        return localidade;
    }

    /**
     * @param localidade
     *            the localidade to set
     */
    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public void save() {
        if (this.id == null) {
            JPA.em().persist(this);
        } else {
            JPA.em().merge(this);
            JPA.em().flush();
        }
        JPA.em().clear();
    }
}
