package base;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import exceptions.AIRCException;
import exceptions.BusinessException;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.Context;
import utils.Constants;

/**
 * Contém métodos comuns a usar nos controllers da app. Todos os controllers têm
 * que extender esta class.
 *
 * @author AIRC
 *
 */
public abstract class ControllerBase extends Controller {

    protected static final ObjectNode EMPTY_JSON = Json.newObject();

    /**
     * Converte o json / queries string presentes no request http num objeto do
     * tipo InterfaceBase.
     *
     * @param clazz
     * @return
     * @throws AIRCException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws BusinessException
     */
    protected <T> T createInputFromRequest(Class<T> clazz)
            throws InstantiationException, IllegalAccessException,
            BusinessException {
        InterfaceBase input;
        try {
            JsonNode jsonNode = request().body().asJson();
            // Se for json
            if (jsonNode != null) {
                input = (InterfaceBase) Json.fromJson(jsonNode, clazz);
                // Caso contrário, deverá ser query string :)
            } else {
                Set<Map.Entry<String, String[]>> entries = request()
                        .queryString().entrySet();
                if (!entries.isEmpty()) {
                    ObjectNode objectNode = Json.newObject();
                    for (Map.Entry<String, String[]> entry : entries) {
                        objectNode.put(entry.getKey(), entry.getValue()[0]);
                    }
                    input = (InterfaceBase) Json.fromJson(objectNode, clazz);
                } else {
                    input = (InterfaceBase) clazz.newInstance();
                }
            }
        } catch (Exception e) {
            throw new BusinessException("PEDIDO_INVALIDO");
        }

        // input.validate();

        return (T) input;
    }
}
