package base;

/**
 * Input generico para um serviço com paginação
 *
 * @author AIRC
 *
 */
public class PaginationInput extends InterfaceBase implements Pagination {

    private int page = 1;
    private int pageSize = 5;
    private String sortBy = "id";
    private String order = "ASC";
    private String filter = "";

    public PaginationInput() {
        super();
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String getSortBy() {
        return sortBy;
    }

    @Override
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    @Override
    public String getOrder() {
        return order;
    }

    @Override
    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String getFilter() {
        return filter;
    }

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
    }
}
