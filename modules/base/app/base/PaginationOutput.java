package base;

import java.util.List;

/**
 * Output genérico para um serviço com paginação.
 * 
 * @author AIRC
 *
 */
public class PaginationOutput extends InterfaceBase implements Pagination {

    private List<EntityBase> pageList;
    private int page;
    private int pageSize;
    private String sortBy;
    private String order;
    private String filter;
    private boolean hasPrev;
    private boolean hasNext;
    private String displayXtoYofZ;

    public PaginationOutput() {
        super();
    }

    public PaginationOutput(Long total, List<EntityBase> pageList,
            Pagination page) {
        super();
        this.pageList = pageList;
        if (page != null) {
            this.page = page.getPage();
            this.pageSize = page.getPageSize();
            this.sortBy = page.getSortBy();
            this.order = page.getOrder();
            this.filter = page.getFilter();
            this.hasPrev = this.page > 1;
            this.hasNext = (total / this.pageSize) >= this.page;

            int start = (this.page - 1) * pageSize + 1;
            int end = start + Math.min(pageSize, pageList.size()) - 1;

            this.displayXtoYofZ = start + "-" + end + " / " + total;
        }
    }

    public List<EntityBase> getPageList() {
        return pageList;
    }

    public void setPageList(List<EntityBase> pageList) {
        this.pageList = pageList;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String getSortBy() {
        return sortBy;
    }

    @Override
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    @Override
    public String getOrder() {
        return order;
    }

    @Override
    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String getFilter() {
        return filter;
    }

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
    }

    public boolean isHasPrev() {
        return hasPrev;
    }

    public void setHasPrev(boolean hasPrev) {
        this.hasPrev = hasPrev;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public String getDisplayXtoYofZ() {
        return displayXtoYofZ;
    }

    public void setDisplayXtoYofZ(String displayXtoYofZ) {
        this.displayXtoYofZ = displayXtoYofZ;
    }
}
