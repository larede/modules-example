package base;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

import enums.EstadoRegistoConverter;
import enums.EstadoRegistoEnum;

/**
 *
 * Contém métodos e campos comuns a todas as entities da app. Todos as Entities
 * têm que extender esta class.
 *
 * @author AIRC
 *
 */
@MappedSuperclass
public abstract class EntityBase extends EntityBaseRef {

    @Basic
    @Column(nullable = false, insertable = true, updatable = true, columnDefinition = "integer default 1")
    @JsonIgnore
    @Convert(converter = EstadoRegistoConverter.class)
    private EstadoRegistoEnum estado = EstadoRegistoEnum.ATIVO;

    public EntityBase() {
    }

    public EstadoRegistoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoRegistoEnum estado) {
        this.estado = estado;
    }
}
