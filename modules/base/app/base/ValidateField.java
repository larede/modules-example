package base;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Interface que representa as anotações possíveis a aplicar aos campos de input
 * dos diversos serviços.
 *
 * @author AIRC
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateField {
    public boolean required() default false;

    public int maxLength() default 0;

    public boolean positive() default false;

    public boolean onlyLetters() default false;

    public boolean onlyNumbers() default false;
}