package base;

import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.fasterxml.jackson.annotation.JsonIgnore;

import play.data.format.Formats;
import play.db.jpa.JPA;
import utils.Constants;

/**
 * Contém métodos e campos comuns a todas as entities da app. Deve ser utilizada
 * APENAS quando NÃO se pretende SOFT DEL uma vez que a sua extensão direta não
 * inclui o campo ESTADO.
 *
 * @author AIRC
 *
 */
@MappedSuperclass
@FilterDef(name = "multitenant", parameters = {
        @ParamDef(name = "tenant_id", type = "long") }, defaultCondition = "tenant_id in (0, :tenant_id)")
@Filters({ @Filter(name = "multitenant") })
public abstract class EntityBaseRef extends InterfaceBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic
    @Formats.DateTime(pattern = Constants.DATE_FORMAT + " HH:mm:ss")
    @Column(name = "timestamp_updated", nullable = false, insertable = true, updatable = true, columnDefinition = "TIMESTAMP default CURRENT_DATE")
    @JsonIgnore
    private Date timestampUpdated = new Date(System.currentTimeMillis());

    @Basic
    @Column(name = "user_updated", nullable = false, insertable = true, updatable = true, length = 20)
    @JsonIgnore
    private String userUpdated;

    @Column(name = "tenant_id", nullable = false, insertable = true, updatable = false)
    @JsonIgnore
    private Long tenantId;

    public EntityBaseRef() {
    }

    /**
     * Permite criar um novo registo e / ou atualizar um já existente
     */
    public void save() {
        if (this.id == null) {
            JPA.em().persist(this);
        } else {
            JPA.em().merge(this);
            JPA.em().flush();
        }
        JPA.em().clear();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTenantId() {
        return tenantId;
    }
}
