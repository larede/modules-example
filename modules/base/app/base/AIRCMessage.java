package base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;

import play.i18n.Messages;
import play.libs.Json;

/**
 * Sempre que há uma exceção na app é devolvido esta class com um código e
 * descrição do erro.
 *
 * @author AIRC
 *
 */
public class AIRCMessage {

    private String code;
    private String message;
    @JsonInclude(Include.NON_NULL)
    private String title;

    public AIRCMessage() {
        super();
    }

    public AIRCMessage(String code, Object... varargs) {
        this.code = code;
        this.message = Messages.get(code, varargs);

        String codeTitle = code + "_TITLE";
        this.title = Messages.get(codeTitle);
        if (this.title.equals(codeTitle)) {
            this.title = null;
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static JsonNode toJson(String code, Object... varargs) {
        return (new AIRCMessage(code, varargs).toJson());
    }

    @JsonIgnore
    public JsonNode toJson() {
        return Json.toJson(this);
    }
}
