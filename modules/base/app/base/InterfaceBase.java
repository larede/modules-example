package base;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import exceptions.BusinessException;
import play.Logger;
import play.libs.Json;

/**
 * O input dos serviços expostos , no caso de existir, é sempre representado por
 * um objeto que extende esta class.
 *
 * @author AIRC
 *
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
public class InterfaceBase {

    @JsonIgnore
    private ObjectNode badRequestFields;

    /**
     * Adiciona erros de validação a um "saco"
     *
     * @param field
     * @param message
     */
    public void addBadRequestField(String field, String message) {
        if (badRequestFields == null) {
            badRequestFields = Json.newObject();
        }

        badRequestFields.put(field, message);
    }

    public void addBadRequestField(AIRCMessage msg)
            throws JsonProcessingException, IOException {
        if (badRequestFields == null) {
            badRequestFields = Json.newObject();
        }
        JsonNode json = msg.toJson();
        badRequestFields = (ObjectNode) (new ObjectMapper()
                .readTree(json.toString()));
    }

    /**
     * Valida os campos do input em função de anotações (Required, MaxLength,
     * etc ...). Se no final foram "encontrados" erros é lançado exceção.
     *
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IOException
     * @throws JsonProcessingException
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws BusinessException
     */
    public boolean validate()
            throws IllegalAccessException, InstantiationException,
            JsonProcessingException, IOException, NoSuchFieldException,
            SecurityException, NoSuchMethodException, IllegalArgumentException,
            InvocationTargetException, BusinessException {
        validateFields(this, "");

        if (badRequestFields != null && badRequestFields.size() > 0) {
            return false;
        }

        return true;
    }

    /**
     * Metodo que permite validar um input do tipo EntityBase, retornando uma
     * AIRCMessage em caso de erro
     *
     * @author AIRC
     *
     * @ input registo a validar (EntityBase)
     *
     * @throws Exception
     *
     */
    public static void validateWithAIRCMessage(InterfaceBase registo)
            throws Exception {
        if (!registo.validate()) {
            AIRCMessage msg = new AIRCMessage();
            msg.setCode(registo.getBadRequestJson().get("code").asText());
            msg.setMessage(registo.getBadRequestJson().get("message").asText());
            msg.setTitle(registo.getBadRequestJson().get("title").asText());
            throw new BusinessException(msg);
        }
    }

    /**
     * Valida em função de anotações que o developer associou aos campos do
     * input a sua conformidade. Função recursiva.
     *
     * @param reference
     * @param prefix
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IOException
     * @throws JsonProcessingException
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws BusinessException
     */
    private void validateFields(Object reference, String prefix)
            throws IllegalAccessException, InstantiationException,
            JsonProcessingException, IOException, NoSuchFieldException,
            SecurityException, NoSuchMethodException, IllegalArgumentException,
            InvocationTargetException, BusinessException {
        for (Field field : reference.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ValidateField.class)) {
                if (!field.getType().isPrimitive()) {
                    ValidateField validateField = field
                            .getAnnotation(ValidateField.class);

                    String fieldId = prefix + field.getName();

                    field.setAccessible(true);
                    if (!validateRequired(reference, fieldId, validateField,
                            field)) {
                        return;
                    }
                    validateMaxLength(reference, fieldId, validateField, field);
                    validatePositive(reference, fieldId, validateField, field);
                    validateOnlyLetters(reference, fieldId, validateField,
                            field);
                    validateOnlyNumbers(reference, fieldId, validateField,
                            field);

                    if (Object.class.isAssignableFrom(field.getType())) {
                        Object value = field.get(reference);
                        if (value != null) {
                            if (List.class.isAssignableFrom(field.getType())) {
                                List<?> values = (List<?>) value;
                                for (int i = 0; i < values.size(); i++) {
                                    validateFields(values.get(i),
                                            fieldId + "[" + i + "].");
                                }
                            } else {
                                validateFields(value, fieldId + ".");
                            }
                        }
                    }
                } else {
                    Logger.error("Não é possível validar tipos primitivos.");
                }
            }
        }
    }

    /**
     * Garante que um campo está preenchido.
     *
     * @param reference
     * @param fieldId
     * @param validateField
     * @param field
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JsonProcessingException
     */
    private boolean validateRequired(Object reference, String fieldId,
            ValidateField validateField, Field field)
            throws IllegalAccessException, JsonProcessingException,
            IOException {
        if (validateField.required()) {
            Object value = field.get(reference);
            if (value == null || ((field.getType()).equals(String.class)
                    && "".equals(value))) {
                // CAMPO_OBRIGATORIO=O campo {0} é de preenchimento obrigatório.
                addBadRequestField(
                        new AIRCMessage("CAMPO_OBRIGATORIO", fieldId));
                return false;
            }
            return true;
        }
        return true;
    }

    /**
     * Garante que um campo não excede o tamnaho máximo definido.
     *
     * @param reference
     * @param fieldId
     * @param validateField
     * @param field
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JsonProcessingException
     */
    private void validateMaxLength(Object reference, String fieldId,
            ValidateField validateField, Field field)
            throws IllegalAccessException, JsonProcessingException,
            IOException {
        if (validateField.maxLength() > 0) {
            Class type = field.getType();
            if (type.equals(String.class) || type.equals(Integer.class)) {

                int maxLength = field.getAnnotation(ValidateField.class)
                        .maxLength();

                int length = 0;
                if (type.equals(String.class)) {
                    length = ((String) field.get(reference)).length();
                } else {
                    if (field.get(reference) != null) {
                        length = (Integer
                                .toString((Integer) field.get(reference)))
                                        .length();
                    }
                }
                if (length > maxLength) {
                    addBadRequestField(
                            new AIRCMessage("TAMANHO_INVALIDO", maxLength));
                }
            } else {
                Logger.error(
                        "Não é possível aplicar o maxLength a tipos que não sejam String ou Integer.");
            }
        }
    }

    /**
     * Garante que um determinado campo é positivo.
     *
     * @param reference
     * @param fieldId
     * @param validateField
     * @param field
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JsonProcessingException
     */
    private void validatePositive(Object reference, String fieldId,
            ValidateField validateField, Field field)
            throws IllegalAccessException, JsonProcessingException,
            IOException {
        if (validateField.positive()) {
            double value = 0;
            boolean isNumber = true;
            try {
                value = Double.parseDouble(field.get(reference).toString());
            } catch (NumberFormatException e) {
                isNumber = false;
                addBadRequestField(new AIRCMessage("VALOR_INVALIDO",
                        String.valueOf(value), fieldId));
            }

            if (isNumber) {
                if (value < 0) {
                    // VALOR_NEGATIVO=O valor ''{0}'' do campo ''{1}'' não é
                    // positivo.
                    addBadRequestField(new AIRCMessage("VALOR_NEGATIVO",
                            String.valueOf(value), fieldId));
                }
            } else {
                Logger.error(
                        "Não é possível aplicar o positive a propriedades não numericas.");
            }
        }
    }

    /**
     * Garante que um determinado campo contem apenas letras.
     *
     * @param reference
     * @param fieldId
     * @param validateField
     * @param field
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws IOException
     * @throws JsonProcessingException
     *
     */
    private void validateOnlyLetters(Object reference, String fieldId,
            ValidateField validateField, Field field)
            throws IllegalArgumentException, IllegalAccessException,
            JsonProcessingException, IOException {
        if (validateField.onlyLetters()) {
            final String regex = "([a-zA-Z]+)";
            Object value = field.get(reference);
            if (value != null && field.getType().equals(String.class)) {
                String valueStr = (String) value;
                if (!(valueStr).matches(regex)) {
                    // VALOR_NAO_TEXTO=O valor do campo {0} não é constituído
                    // apenas por letras.
                    addBadRequestField(new AIRCMessage("VALOR_NAO_TEXTO",
                            field.getName()));
                }
            } else {
                Logger.error(
                        "Não é possível aplicar o validateOnlyLetters a campos nulos.");
            }
        }
    }

    /**
     * Garante que um determinado campo é numérico.
     *
     * @param reference
     * @param fieldId
     * @param validateField
     * @param field
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JsonProcessingException
     */
    private void validateOnlyNumbers(Object reference, String fieldId,
            ValidateField validateField, Field field)
            throws IllegalAccessException, JsonProcessingException,
            IOException {
        if (validateField.onlyNumbers()) {
            double value = 0;
            try {
                value = Double.parseDouble(field.get(reference).toString());
            } catch (NumberFormatException e) {
                addBadRequestField(new AIRCMessage("VALOR_INVALIDO",
                        String.valueOf(value), fieldId));
            }
        }
    }

    @JsonIgnore
    public JsonNode getBadRequestJson() {
        return Json.parse(Json.stringify(badRequestFields));
    }
}
