package base;

/**
 * Interface que permite definir os campos obrigatórios para a implementação de
 * paginação
 * 
 * @author AIRC
 *
 */
public interface Pagination {

    public int getPage();

    public void setPage(int page);

    public int getPageSize();

    public void setPageSize(int pageSize);

    public String getSortBy();

    public void setSortBy(String sortBy);

    public String getOrder();

    public void setOrder(String order);

    public String getFilter();

    public void setFilter(String filter);
}