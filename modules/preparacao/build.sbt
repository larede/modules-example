name := """preparacao"""

version := "1.0-SNAPSHOT"

lazy val preparacao = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)
